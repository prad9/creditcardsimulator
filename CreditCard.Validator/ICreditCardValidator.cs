﻿namespace CreditCard.Validator
{
    public interface ICreditCardValidator
    {
        bool IsValid(CreditCard creditCard);
    }
}