﻿namespace CreditCard.Validator
{
    public class CreditCard
    {
        public string Number { get; set; }
        public CreditCardType Type { get; set; }
    }
}