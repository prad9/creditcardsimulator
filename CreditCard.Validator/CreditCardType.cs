﻿using System;

namespace CreditCard.Validator
{
    [Flags]
    public enum CreditCardType
    {
        Visa = 1,
        MasterCard = 2,
        Diners = 4,
        AmericanExpress = 8,
        Unknown = 16
    }
}