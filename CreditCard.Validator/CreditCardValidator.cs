﻿using System.Text.RegularExpressions;

namespace CreditCard.Validator
{
    public class CreditCardValidator : ICreditCardValidator
    {
        public bool IsValid(CreditCard creditCard)
        {
            var cardType = GetCardType(creditCard.Number);
            return cardType != CreditCardType.Unknown && creditCard.Type == cardType;
        }

        protected CreditCardType GetCardType(string creditCardNumber)
        {
            if (IsCardTypeVisa(creditCardNumber)) return CreditCardType.Visa;
            if (IsCardTypeMaster(creditCardNumber)) return CreditCardType.MasterCard;
            if (IsCardTypeAmericanExpress(creditCardNumber)) return CreditCardType.AmericanExpress;
            if (IsCardTypeDiners(creditCardNumber)) return CreditCardType.Diners;
            return CreditCardType.Unknown;
        }

        private bool IsCardTypeDiners(string creditCardNumber)
        {
            return Regex.IsMatch(creditCardNumber, "^3(?:0[0-5]|[68][0-9])[0-9]{11}$");
        }

        private bool IsCardTypeAmericanExpress(string creditCardNumber)
        {
            return Regex.IsMatch(creditCardNumber, "^3[47][0-9]{13}$");
        }

        private bool IsCardTypeMaster(string creditCardNumber)
        {
            return Regex.IsMatch(creditCardNumber, "^5[1-5][0-9]{14}$");
        }

        private bool IsCardTypeVisa(string creditCardNumber)
        {
            return Regex.IsMatch(creditCardNumber, "^4[0-9]{12}(?:[0-9]{3})?$");
        }
    }
}