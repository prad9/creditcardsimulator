﻿using NUnit.Framework;

namespace CreditCard.Validator.Tests
{
    [TestFixture]
    public class GetCardTypeTests
    {
        [SetUp]
        public void Setup()
        {
            _validator = new CreditCardValidatorTest();
        }

        private CreditCardValidatorTest _validator;

        [Test]
        public void When_InValid_CardNumber_Return_Unknown()
        {
            //Arrange 
            const string visaCardNumber = "1234567890123456";

            //Act
            var type = _validator.GetCardTypeTest(visaCardNumber);

            Assert.IsTrue(type.HasFlag(CreditCardType.Unknown));
        }

        [Test]
        public void When_ValidAmericalExpress_CardNumber_Return_AmericanExpress()
        {
            //Arrange 
            const string visaCardNumber = "341234567890123";

            //Act
            var type = _validator.GetCardTypeTest(visaCardNumber);

            Assert.IsTrue(type.HasFlag(CreditCardType.AmericanExpress));
        }

        [Test]
        public void When_ValidDiners_CardNumber_Return_Diners()
        {
            //Arrange 
            const string visaCardNumber = "30012345678901";

            //Act
            var type = _validator.GetCardTypeTest(visaCardNumber);

            Assert.IsTrue(type.HasFlag(CreditCardType.Diners));
        }

        [Test]
        public void When_ValidMaster_CardNumber_Return_Master()
        {
            //Arrange 
            const string visaCardNumber = "5123456789012345";

            //Act
            var type = _validator.GetCardTypeTest(visaCardNumber);

            Assert.IsTrue(type.HasFlag(CreditCardType.MasterCard));
        }

        [Test]
        public void When_ValidVisa_CardNumber_Return_Visa()
        {
            //Arrange 
            const string visaCardNumber = "4123456789012345";

            //Act
            var type = _validator.GetCardTypeTest(visaCardNumber);

            Assert.IsTrue(type.HasFlag(CreditCardType.Visa));
        }

        [Test]
        public void When_Visa_CardNumber_WithInvalidLength_Return_Unknown()
        {
            //Arrange 
            const string visaCardNumber = "41234567890";

            //Act
            var type = _validator.GetCardTypeTest(visaCardNumber);

            Assert.IsTrue(type.HasFlag(CreditCardType.Unknown));
        }
    }
}