﻿using NUnit.Framework;

namespace CreditCard.Validator.Tests
{
    [TestFixture]
    public class IsValidTests
    {
        [SetUp]
        public void Setup()
        {
            _creditCardTypeChecker = new CreditCardValidator();
            _creditCard = new CreditCard();
        }

        private ICreditCardValidator _creditCardTypeChecker;
        private CreditCard _creditCard;

        [Test]
        [TestCase("41239012", CreditCardType.Visa)]
        [TestCase("5123012345", CreditCardType.MasterCard)]
        [TestCase("341890123", CreditCardType.AmericanExpress)]
        [TestCase("300178901", CreditCardType.Diners)]
        public void When_InValid_Number_And_ValidType_Return_False(string number, CreditCardType type)
        {
            //Arrange
            _creditCard.Number = number;
            _creditCard.Type = type;

            //Act
            var isValid = _creditCardTypeChecker.IsValid(_creditCard);

            Assert.IsFalse(isValid);
        }

        [Test]
        [TestCase("4123456789012", CreditCardType.MasterCard)]
        [TestCase("5123456789012345", CreditCardType.Visa)]
        [TestCase("341234567890123", CreditCardType.Diners)]
        [TestCase("30012345678901", CreditCardType.Unknown)]
        public void When_Valid_Number_And_InvalidType_Return_False(string number, CreditCardType type)
        {
            //Arrange
            _creditCard.Number = number;
            _creditCard.Type = type;

            //Act
            var isValid = _creditCardTypeChecker.IsValid(_creditCard);

            Assert.IsFalse(isValid);
        }

        [Test]
        public void When_Valid_Number_And_UnknowwnType_Return_False()
        {
            //Arrange
            _creditCard.Number = "4123456789012";
            _creditCard.Type = CreditCardType.Unknown;

            //Act
            var isValid = _creditCardTypeChecker.IsValid(_creditCard);

            Assert.IsFalse(isValid);
        }

        [Test]
        [TestCase("4123456789012", CreditCardType.Visa)]
        [TestCase("5123456789012345", CreditCardType.MasterCard)]
        [TestCase("341234567890123", CreditCardType.AmericanExpress)]
        [TestCase("30012345678901", CreditCardType.Diners)]
        public void When_Valid_Number_And_ValidType_Return_True(string number, CreditCardType type)
        {
            //Arrange
            _creditCard.Number = number;
            _creditCard.Type = type;

            //Act
            var isValid = _creditCardTypeChecker.IsValid(_creditCard);

            Assert.IsTrue(isValid);
        }
    }
}